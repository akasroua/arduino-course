#!/bin/sh

if [ $# -lt 1 ]; then
    echo "Usage: compile.sh <Project>"
fi

project=$1

arduino-cli compile --fqbn arduino:avr:uno "$project" && arduino-cli upload -p /dev/ttyACM0 --fqbn arduino:avr:uno "$project"
