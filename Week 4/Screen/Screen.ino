#include <Wire.h>
#include <LiquidCrystal_I2C.h>

//CREAMOS EL OBJETO LCD CON LA DIRECCIÓN ESCANEADA 0X27, 16 COLUMNAS Y 2 FILAS
LiquidCrystal_I2C lcd(0x27,16,2);

void setup() {
  // Inicializar el LCD
  lcd.init();
  //Encender la luz de fondo.
  lcd.backlight();
  // Escribimos el Mensaje en el LCD.
  lcd.print("Hola Mundo");
}

void loop() {
  //Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  lcd.setCursor(0, 1);
  // Escribimos el número de segundos transcurridos
  lcd.print(millis()/1000);
  lcd.print(" Segundos");
  delay(100);
}
