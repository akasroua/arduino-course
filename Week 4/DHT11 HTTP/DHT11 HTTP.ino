#include <DHT.h>
#include <DHT_U.h>
#include <ESP8266WiFi.h>

#define DHTPIN D2
#define DHTTYPE DHT11

const char* ssid     = "FundamentosRedes";
const char* password = "00000005";

const char* host = "192.168.10.155";
const uint16_t port = 80;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
dht.begin();

Serial.println();
Serial.println();
Serial.print("CONECTANDO A.... ");
Serial.println(ssid);

//WiFi.mode(WIFI_STA);
WiFi.begin(ssid, password);

while (WiFi.status() != WL_CONNECTED) {
 delay(500);
 Serial.print(".");
}

Serial.println("");
Serial.println("WiFi CONECTADA");
Serial.println("IP address: ");
Serial.println(WiFi.localIP());

}

void loop() {
// obtenemos valores dht11

delay(2000);
float humedad = dht.readHumidity();
float temperatura = dht.readTemperature();
Serial.print(F("Humedad:"));
Serial.print(humedad);
Serial.print(F("% Temperatura: :"));
Serial.print(temperatura);
Serial.println(F("ºC"));

Serial.print("CONECTANDO A..");
Serial.println(host);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    return;
  }

  //Creamos la URL para mandar al servidor 
  String url = "/NODEMCU/DHT11/DHT11.php";
  String Temperatura = "?Temperatura=";
  String Humedad = "&Humedad=";

  client.print(String("GET ") + url + Temperatura + temperatura + Humedad + humedad + " HTTP/1.1\r\n" +
              "Host: " + host +"\r\n" +
              "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0){
    if (millis() - timeout > 5000) {
      Serial.println(">>> Servidor Timeout !!!");  
      client.stop();
      return;
    }  
  }

  while (client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);  
  }

  Serial.println();
  Serial.println("Cerrando conexión");

  delay(60000);
  
  
  // put your main code here, to run repeatedly:


}
