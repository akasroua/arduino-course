#include <ESP8266WiFi.h>

const char* ssid     = "FundamentosRedes";
const char* password = "00000005";

void setup() {
  Serial.begin(115200);
  Serial.println("");
  Serial.print("CONECTANDO A SSID:");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("Conectado. Dirección IP: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  if( WiFi.status() == WL_CONNECTED){
    Serial.println("");
    Serial.print("Conectado. Dirección IP: ");
    Serial.println(WiFi.localIP());
  }
  else{
    Serial.println("");
    Serial.println("WiFi no conectada");
  }
  delay(1000);
}
