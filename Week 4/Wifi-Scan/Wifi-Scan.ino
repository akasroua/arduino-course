#include "ESP8266WiFi.h"

void setup() {
  Serial.begin(115200);
 //CONFIGURAMOS LA WIFI EN MODO ESTACIÓN Y DESCONECTAMOS DEL ACCES POINT SI PREVIAMENTE ESTABA CONECTADO A UN AP.
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  Serial.println("Configuración Wifi realizada");
}

void loop() {
  Serial.println("Escaneo de Redes Wifi:");
  // La función scanNetwoks() nos devuelve el número de redes encontradas.
  int n = WiFi.scanNetworks();
  Serial.println("Escaneo Realizado");
  // si n=0 es que no hemos encontrado ninguna red wifi
  if (n == 0) {
    Serial.println("Ninguna red encontrada");
  }
 else {
    Serial.print(n);
    Serial.println(" Redes Wifi encontradas:");
    for (int i = 0; i < n; ++i) {
      //Imprimimos el SSDI de cada red wifi encontrada
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
  // EspEramos 50.000 ms antes de repetir el escaneo. Si quisiéramos 
  // realizar un solo escaneo deberíamos poner el código del look en el setup.
  delay(50000);
}
