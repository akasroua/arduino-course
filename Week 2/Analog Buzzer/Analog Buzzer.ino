// Analog Buzzer
int Buzzer = 5;

void setup() {
  pinMode(Buzzer, OUTPUT);
}

void loop() {
  int val = analogRead(A5);
  digitalWrite(Buzzer, HIGH);
  delay(val);
  digitalWrite(Buzzer, LOW);
  delay(val);
}
