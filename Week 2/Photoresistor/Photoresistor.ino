// Photoresistor
int sensorPIN = A0;
int LED = 9;
int sensorValue = 0;
int LEDValue = 0;
void setup() {
  pinMode(LED, OUTPUT);
}

void loop() {
  sensorValue = analogRead(sensorPIN);
  LEDValue = map(sensorValue, 0, 1023, 0, 255);
  analogWrite(LED, LEDValue);
  delay(100);
}
