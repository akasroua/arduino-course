// Analog Blink
int LED = 13;

void setup() {
  pinMode(LED, OUTPUT);
}

void loop() {
  int val = analogRead(A5);
  digitalWrite(LED, HIGH);
  delay(val);
  digitalWrite(LED, LOW);
  delay(val);
}
