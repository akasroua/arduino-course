// RGB LED
int red = 3;
int blue = 5;
int green = 6;

void setup() {
  pinMode(red, OUTPUT);
  pinMode(blue, OUTPUT);
  pinMode(green, OUTPUT);
}

void loop() {
  analogWrite(red, random(0,255));
  delay(200);
  analogWrite(blue, random(0,255));
  delay(200);
  analogWrite(green, random(0,255));
  delay(200);
}
