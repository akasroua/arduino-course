// Analog LED Strip
int LED1 = 7;
int LED2 = 6;
int LED3 = 5;
int LED4 = 4;
int LED5 = 3;
void setup() {
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(LED5, OUTPUT);
}

void loop() {
  int analog = analogRead(A0);
  analog = map(analog, 0, 1023, 0, 5);

  digitalWrite(LED1, LOW);
  digitalWrite(LED2, LOW);
  digitalWrite(LED3, LOW);
  digitalWrite(LED4, LOW);
  digitalWrite(LED5, LOW);

  if(analog > 0) {
    digitalWrite(LED1, HIGH);
  }
  if (analog > 1){
    digitalWrite(LED2, HIGH);
  }
  if (analog > 2){
    digitalWrite(LED3, HIGH);
  }
  if (analog > 3){
    digitalWrite(LED4, HIGH);
  }
  if (analog > 4){
    digitalWrite(LED5, HIGH);
  }

  delay(50);
}
