// LED Intensity Control
int LED = 9;
int intensity = 0;
int potentionmeterValue = 0;
int potentiometerPin = A0;
void setup() {
  pinMode(LED, OUTPUT);
}

void loop() {
  potentionmeterValue = analogRead(potentiometerPin);
  intensity = map(potentionmeterValue, 0, 1023, 0, 255);
  analogWrite(LED, intensity);
  delay(10);
}
