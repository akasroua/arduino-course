// RGB with potentiometers
int red = 3;
int blue = 5;
int green = 6;
int redValue = 0;
int blueValue = 0;
int greenValue = 0;
void setup() {
  pinMode(red, OUTPUT);
  pinMode(blue, OUTPUT);
  pinMode(green, OUTPUT);
}

void loop() {
  redValue = map(analogRead(A0), 0, 1023, 0, 255);
  greenValue = map(analogRead(A1), 0, 1023, 0, 255);
  blueValue = map(analogRead(A2), 0, 1023, 0, 255);

  analogWrite(red, redValue);
  analogWrite(green, greenValue);
  analogWrite(blue, blueValue);
}
