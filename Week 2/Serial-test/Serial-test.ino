// Serial port test
int time = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("Elapsed time");
}

void loop() {
  Serial.println(time);
  time++;
  delay(1000);
}
