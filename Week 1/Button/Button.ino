//Button
#define BUTTON 8
#define LED 3
bool botonEncendido = false;

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT);
}

void loop() {
  botonEncendido = digitalRead(BUTTON);

  if(botonEncendido){
    digitalWrite(LED, HIGH);
  } else {
    digitalWrite(LED, LOW);
  }
}
