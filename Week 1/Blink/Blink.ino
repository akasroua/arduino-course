// Blink
void setup() {
  pinMode(7, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(3, OUTPUT);
}

void loop() {
  int pin1 = 7;
  int pin2 = 5;
  int pin3 = 3;
  int time = 100;
  digitalWrite(pin1, HIGH);
  delay(time);
  digitalWrite(pin1, LOW);
  delay(time);
  digitalWrite(pin2, HIGH);
  delay(time);
  digitalWrite(pin2, LOW);
  delay(time);
  digitalWrite(pin3, HIGH);
  delay(time);
  digitalWrite(pin3, LOW);
}
