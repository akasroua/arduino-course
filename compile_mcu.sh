#!/bin/sh

if [ $# -lt 1 ]; then
    echo "Usage: compile.sh <Project>"
fi

project=$1

arduino-cli compile --fqbn esp8266:esp8266:nodemcuv2 "$project" && arduino-cli upload -p /dev/ttyUSB0 --fqbn esp8266:esp8266:nodemcuv2 "$project"
