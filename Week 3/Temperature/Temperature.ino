#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN 2
#define DHTTYPE DHT11

int red = 7;
int green = 5;
int yellow = 6;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(yellow, OUTPUT);
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  delay(2000);
  float humidity = dht.readHumidity();
  float temp = dht.readTemperature();

  Serial.print(F("Humedad: "));
  Serial.print(humidity);
  Serial.print(F("% Temperatura: "));
  Serial.print(temp);
  Serial.print(F("ºC "));

  if(temp >= 28){
    digitalWrite(red, HIGH);
    digitalWrite(green, LOW);
    digitalWrite(yellow, LOW);
  } else if (temp >= 20 && temp <= 26){
    digitalWrite(red, LOW);
    digitalWrite(green, HIGH);
    digitalWrite(yellow, LOW);
  } else {
    digitalWrite(red, LOW);
    digitalWrite(green, LOW);
    digitalWrite(yellow, HIGH);
  }
}
