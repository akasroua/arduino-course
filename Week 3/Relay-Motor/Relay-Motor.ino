long val;
int spin;

void setup() {
  Serial.begin(9600);
  Serial.println("Trolaso");
}

void loop() {
  val = analogRead(A0);
  Serial.println("valor: ");
  Serial.println(val);
  spin = map(val, 0, 1023, 0, 255);

  analogWrite(9, spin);
  Serial.println("giro: ");
  Serial.println(spin);
  delay(500);
}
