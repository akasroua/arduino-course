#include <DHT.h>
#include <DHT_U.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F, 16, 2);

#define DHTPIN 2
#define DHTTYPE DHT11

int rojo = 7;
int verde = 5;
int amarillo = 6;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  pinMode(rojo, OUTPUT);
  pinMode(verde, OUTPUT);
  pinMode(amarillo, OUTPUT);

  Serial.begin(9600);
  dht.begin();

  lcd.init();
  lcd.backlight();
}

void loop() {
  delay(2000);
  float humedad = dht.readHumidity();
  float temperatura = dht.readTemperature();

  lcd.setCursor(0,0);
  lcd.print(F("Humedad: "));
  lcd.print(humedad);
  lcd.print("%");

  lcd.setCursor(0,1);
  lcd.print(F("Temp: "));
  lcd.print(temperatura);
  Serial.print(F("C "));

  if (temperatura >= 28){
  digitalWrite (rojo, HIGH);
  digitalWrite (verde, LOW);
  digitalWrite (amarillo, LOW);
  }else if(temperatura >= 20 && temperatura <= 26){
  digitalWrite (rojo, LOW);
  digitalWrite (verde, HIGH);
  digitalWrite (amarillo, LOW);
  }else{
  digitalWrite (rojo, LOW);
  digitalWrite (verde, LOW);
  digitalWrite (amarillo, HIGH);
  }
}
