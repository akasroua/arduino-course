#include <Servo.h>

Servo myservo;

int potentiometer = A5;
int val;

void setup() {
  myservo.attach(6);
  Serial.begin(9600);
}

void loop() {
  val = analogRead(potentiometer);

  Serial.print("Valor potenciómetro: ");
  Serial.print(val);

  val = map(val, 0, 1023, 0, 180);

  Serial.print(" - Ángulo: ");
  Serial.print(val);

  myservo.write(val);
  delay(15);
}
