#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F, 16, 2);

const int SIZE = 8;

void setup() {
  lcd.init();
  lcd.backlight();
  lcd.print("Iconos loko");

  byte corazon[SIZE] = {
    0b00000,
    0b00000,
    0b01010,
    0b11111,
    0b11111,
    0b11111,
    0b01110,
    0b00100
  };

  byte campana[SIZE] = {
    B00000,
    B00100,
    B01110,
    B01110,
    B01110,
    B11111,
    B00100,
    B00000
  };

  lcd.createChar(0, corazon);
  lcd.createChar(1, campana);
}

void loop() {
  lcd.setCursor(0, 1);
  lcd.write(byte(0));
  delay(100);
  lcd.write(byte(1));
  delay(100);
}
